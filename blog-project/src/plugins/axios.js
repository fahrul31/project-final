   // in Vue 2
import Vue from 'vue' 
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)